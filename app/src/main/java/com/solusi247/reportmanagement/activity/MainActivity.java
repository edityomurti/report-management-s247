package com.solusi247.reportmanagement.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.solusi247.reportmanagement.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
